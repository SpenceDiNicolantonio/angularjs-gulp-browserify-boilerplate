'use strict';

var istanbul = require('browserify-istanbul');
var isparta = require('isparta');

module.exports = function(config) {

	config.set({

		basePath: '../',
		frameworks: ['jasmine', 'browserify'],
		preprocessors: {
			'app/**/*.js': ['browserify', 'babel', 'coverage']
		},
		browsers: ['Chrome'],
		reporters: ['progress', 'coverage'],

		autoWatch: true,

		browserify: {
			debug: true,
			transform: [
				'partialify',
				istanbul({
					instrumenter: isparta,
					ignore: ['**/node_modules/**', '**/test/**']
				})
			]
		},

		proxies: {
			'/': 'http://localhost:9876/'
		},

		urlRoot: '/__karma__/',

		files: [
			// 3rd-party resources
			'node_modules/angular/angular.js',
			'node_modules/angular-mocks/angular-mocks.js',

			// app-specific code
			'app/main.js',

			// test files
			'test/unit/**/*.js'
		]

	});

};