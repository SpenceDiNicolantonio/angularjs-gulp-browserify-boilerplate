'use strict';

module.exports = {

	'serverport': 3000,

	'styles': {
		'src' : 'app/styles/**/*.scss',
		'dest': 'build/css'
	},

	'scripts': {
		'src' : 'app/**/*.js',
		'dest': 'build/js'
	},

	'images': {
		'src' : 'app/images/**/*',
		'dest': 'build/images'
	},

	'fonts': {
		'src' : ['app/fonts/**/*'],
		'dest': 'build/fonts'
	},

	'views': {
		'src': 'app/**/*.html',
		'dest': 'build/views'
	},

	'gzip': {
		'src': 'build/**/*.{html,xml,json,css,js,js.map}',
		'dest': 'build/',
		'options': {}
	},

	'dist': {
		'root'	: 'build'
	},

	'browserify': {
		'entries'	 : ['./app/main.js'],
		'bundleName': 'main.js',
		'sourcemap' : true
	},

	'test': {
		'karma': 'test/karma.conf.js',
		'protractor': 'test/protractor.conf.js'
	}

};
