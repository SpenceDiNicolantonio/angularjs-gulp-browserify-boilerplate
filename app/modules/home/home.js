'use strict';

var angular = require('angular');

var HomeController = require('./home-controller');
var ClickDirective = require('./click-directive');
var ExampleService = require('./example-service');
var HomeConfig = require('./home-config');
var HomeRun = require('./home-run');


// Module
var home = angular.module('app.home', []);

// Controllers
home.controller('HomeController', HomeController);

// Services
home.service('Example', ExampleService);

// Directives
home.directive('click', ClickDirective);

// Execute configuration and main routine
home.config(HomeConfig);
home.run(HomeRun);


module.exports = home;