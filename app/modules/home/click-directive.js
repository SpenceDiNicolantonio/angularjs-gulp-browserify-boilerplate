'use strict';

/**
 * @ngInject
 */
function ClickDirective() {

	return {
		restrict: 'EA',
		link: function(scope, element) {
			element.on('click', function() {
				console.log('element clicked');
			});
		}
	};

}

module.exports = ClickDirective;