'use strict';

var angular = require('angular');

var SharedService = require('./shared-service');
var SharedDirective = require('./shared-directive');
var CommonConfig = require('./common-config');
var CommonRun = require('./common-run');


// Module
var common = angular.module('app.common', []);

// Services
common.service('Shared', SharedService);

// Directives
common.directive('shared', SharedDirective);

// Execute configuration and main routine
common.config(CommonConfig);
common.run(CommonRun);


module.exports = common;