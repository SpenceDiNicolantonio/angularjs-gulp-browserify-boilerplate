'use strict';

var angular = require('angular');
require('angular-ui-router');


// Angular modules
var common = require('./modules/common/common.js');
var home = require('./modules/home/home.js');


// Create and bootstrap application
angular.element(document).ready(function() {

	// Mount on window for testing
	window.app = angular.module('app', [
		'ui.router',
		common.name,
		home.name
	]);

	angular.module('app').constant('AppSettings', require('./constants'));
	angular.module('app').config(require('./routes'));
	angular.module('app').run(require('./run'));

	angular.bootstrap(document, ['app']);

});